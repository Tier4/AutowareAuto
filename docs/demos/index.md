Demonstrations {#demos}
=========

Autoware.Auto includes a growing number of demonstrations.
These are used to demonstrate specific capabilities of the software, such as the ability to park a car or the ability to drive a route through an urban setting.
Most demonstrations can be used on both a real car and in a simulator.
Each article below contains instructions on how to set up, launch and control one demonstration.
For demonstrations that work on a real car, the hardware requirements are also described.

The demonstration documentation assumes a working knowledge of how to use Autoware.Auto.
For articles on how to achieve specific tasks with Autoware.Auto, see our [HowTo]{@ref howto} and [tutorials]{@ref tutorials} categories.

- @subpage avpdemo
